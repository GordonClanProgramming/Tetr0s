﻿//Evan Gordon
/*
 * wpf code for all of the on screen elements. 
 * Note within here is code for wmplib which uses a song, i did not create the song and by no means claim it as my own.
 * The song i am using was a free download from this location:
 * https://soundcloud.com/incredfx/tetris-theme-incredfx-electro
 * 
 * However all of the art for this game i made myself (minus the backgrounds and such that wpf generated for me)
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;
using System.Windows.Threading;

namespace Tetr0s
{
    /// <summary>
    /// Routed commands used for linking bound keys to functions
    /// </summary>
    public static class Command
    {
        public static RoutedCommand newGameCommand = new RoutedCommand();
        public static RoutedCommand continueCommand = new RoutedCommand();
        public static RoutedCommand pauseCommand = new RoutedCommand();
        public static RoutedCommand closeCommand = new RoutedCommand();
        public static RoutedCommand aboutCommand = new RoutedCommand();
        public static RoutedCommand rulesCommand = new RoutedCommand();
        public static RoutedCommand upCommand = new RoutedCommand();
        public static RoutedCommand downCommand = new RoutedCommand();
        public static RoutedCommand leftCommand = new RoutedCommand();
        public static RoutedCommand rightCommand = new RoutedCommand();
        public static RoutedCommand spaceCommand = new RoutedCommand();
        public static RoutedCommand incrementLevelCommand = new RoutedCommand();
        public static RoutedCommand saveCommand = new RoutedCommand();
        public static RoutedCommand loadCommand = new RoutedCommand();

    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
        WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        int milliseconds = 500;
        Level gameLevel = new Level();
        Board gameBoardData;
        bool isPlaying = false;
        string[] setting = new string[0];
        int highScore = 0;

        public MainWindow()
        {
            InitializeComponent();
            player.URL = "tetrisTheme.mp3";
            gameBoardData = new Board(gameBoard);
            pauseMenu.IsEnabled = false;
            continueMenu.IsEnabled = false;
            isPlaying = false;
            timer.Tick += timer_Tick;
            string settingsText = "";
            if (File.Exists("settings.txt"))
            {
                StreamReader reader = new StreamReader("settings.txt");
                settingsText = reader.ReadLine();
                reader.Close();
                setting = settingsText.Split(',');
                highScore = int.Parse(setting[0]);

            }
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            if (newGameMenu.IsEnabled)
            {
                player.controls.play();
                timer.Start();
                pauseMenu.IsEnabled = true;
                continueMenu.IsEnabled = false;
                gameBoardData = new Board(gameBoard);
                gameBoard.Children.Clear();
                setScoreAndLevel(sender, e);
                gameBoardData.spawn(gameBoard);
                newGameMenu.IsEnabled = false;
                isPlaying = true;
                milliseconds = 500;
                timer.Interval = TimeSpan.FromMilliseconds(milliseconds);
                loseText.Visibility = System.Windows.Visibility.Hidden;
                gameLevel = new Level();
                saveMenu.IsEnabled = false;
                loadMenu.IsEnabled = false;

            }
        }

        private void Continue_Click(object sender, RoutedEventArgs e)
        {
            if (continueMenu.IsEnabled)
            {
                timer.Start();//maybe delay this?
                pauseMenu.IsEnabled = true;
                continueMenu.IsEnabled = false;
                newGameMenu.IsEnabled = false;
                isPlaying = true;
                saveMenu.IsEnabled = false;
                loadMenu.IsEnabled = false;
            }
        }

        private void Pause_Click(object sender, RoutedEventArgs e )
        {
            if (pauseMenu.IsEnabled)
            {
                pauseMenu.IsEnabled = false;
                continueMenu.IsEnabled = true;
                newGameMenu.IsEnabled = true;
                isPlaying = false;
                timer.Stop();
                saveMenu.IsEnabled = true;
                loadMenu.IsEnabled = true;
            }
        }

        private void endGame(object sender, EventArgs e)
        {
            if (gameLevel.Score > highScore)
            {
                highScore = gameLevel.Score;
                setScoreAndLevel(sender, e);
            }
            player.controls.stop();
            pauseMenu.IsEnabled = false;
            continueMenu.IsEnabled = false;
            newGameMenu.IsEnabled = true;
            isPlaying = false;
            timer.Stop();
            loseText.Visibility = System.Windows.Visibility.Visible;
            saveMenu.IsEnabled = true;
            loadMenu.IsEnabled = true;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            pauseMenu.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            if (gameLevel.Score > highScore)
            {
                highScore = gameLevel.Score;
                setScoreAndLevel(sender, e);
            }
            player.controls.stop();
            StreamWriter sw = new StreamWriter("settings.txt");
            sw.WriteLine(highScore + ",0,0");//extra values are for values to be added later
            sw.Close();
            MessageBoxResult r = MessageBox.Show("Are you sure?", "Exit", MessageBoxButton.OKCancel, new MessageBoxImage());
            if (r == MessageBoxResult.OK)
            {
                this.Close();
            }
        }

        private void Rules_Click(object sender, RoutedEventArgs e)
        {
            pauseMenu.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            MessageBox.Show("How To Play: In the File menu, click New Game to begin a new game of tetr0s.\n "
                + "You may also load a previously saved game via the load button, and then entering the name of the saved file\n" +
                "To play the game, use arrow keys to rotate and move the current shape before it reaches the ground!\n" + 
                "You can also use space to send the current shape to the ground. Also there is a special hidden key\n" +
                "Will enable you to skip levels, you sneaky devil you.\n"+ "Happy Stacking!");
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            pauseMenu.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            AboutWindow aw = new AboutWindow();
            aw.ShowDialog();
        }

        private void setScoreAndLevel(object sender, EventArgs e)
        {
            scoreAndLevel.Content = "High Score: \n" + highScore + "\nScore: " + gameLevel.Score + "\nLevel: " + gameLevel.LevelNum ;
            if (gameLevel.checkIfLevelIncreased())
            {
                milliseconds = (int)(((double)milliseconds * 0.75) + 0.5);
                timer.Interval = TimeSpan.FromMilliseconds(milliseconds);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            gameLevel.CompletedRows = gameBoardData.draw(gameBoard);//draw everything here  
            setScoreAndLevel(sender, e);
            //set new timer time~~~~~~~~~~
            if (gameBoardData.GameOver)
            {
                endGame(sender, e);
            }
        }

        private void Up_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameBoardData.rotate(Turn.LEFT, gameBoard);
            }
        }

        private void Down_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameBoardData.rotate(Turn.RIGHT, gameBoard);
            }
        }

        private void Left_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameBoardData.move(Direction.LEFT, gameBoard);
            }
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameBoardData.move(Direction.RIGHT, gameBoard);
            }
        }

        private void Space_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameBoardData.maxLower(gameBoard);
            }
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            if (isPlaying)
            {
                gameLevel.LevelNum = 1;
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (!isPlaying)
            {
                SaveWindow sw = new SaveWindow();
                sw.ShowDialog();
                string text = sw.Text;

                try//saving
                {
                    StreamWriter writer = new StreamWriter(text + ".txt");
                    writer.WriteLine(gameLevel.LevelNum + "," + gameLevel.Score);
                    gameBoardData.save(ref writer);
                    writer.Close();
                }
                catch (FileNotFoundException)
                {

                }
            }
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (!isPlaying)
            {
                loadWindow lw = new loadWindow();
                lw.ShowDialog();
                string fileName = lw.Text;
                try
                {
                    StreamReader reader = new StreamReader(fileName + ".txt");
                    //code edited from here: http://stackoverflow.com/questions/7425963/how-to-read-values-from-a-comma-separated-file

                    string line = reader.ReadLine();
                    string[] lvlInfo = line.Split(',');

                    NewGame_Click(sender, e);

                    gameBoardData.clear(gameBoard);
                    gameLevel = new Level(int.Parse(lvlInfo[0]), int.Parse(lvlInfo[1]));
                    gameBoardData.load(ref reader, gameBoard);
                    gameBoardData.spawn(gameBoard);

                    reader.Close();
                    Pause_Click(sender, e);
                }
                catch(FileNotFoundException)
                {

                }
            }
        }

        private void fileMenu_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
