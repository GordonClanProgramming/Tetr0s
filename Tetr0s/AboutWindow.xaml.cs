﻿//Evan Gordon
//The Actual app icon doesnt show up when opened via visual studios for some reason
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Tetr0s
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            string bit = "";
            if (IntPtr.Size == 4)
            {
                bit = "32bit";// 32-bit
            }
            else if (IntPtr.Size == 8)
            {
                bit = "64bit";// 64-bit
            }
            aboutText.Content = "Developed by: Evan Gordon\n Version Number: " + Assembly.GetExecutingAssembly().GetName().Version.ToString() +
                "\nThis program is running .NET framework version " + Environment.Version + " and is running in " + bit + ".\n"
                + "Audio is from: https://soundcloud.com/incredfx/tetris-theme-incredfx-electro";
        }

    }
}
