﻿//Evan Gordon

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;

namespace Tetr0s
{
    enum Direction {UP, DOWN, LEFT, RIGHT };
    class Tile
    {
        private bool isEmpty;
        public bool IsEmpty
        {
            get {return isEmpty;}
            set { isEmpty = value; }
        }

        private bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        private List<Image> sprite = new List<Image>();
        private int it;
        public Tile()
        {
            it = 0;
            isEmpty = true;
            isActive = false;
        }

        public Tile(string fileName, Point pos)
        {
            it = 0;
            sprite.Add(new Image());
            sprite[0].Source = new ImageSourceConverter().ConvertFromString(fileName) as ImageSource;
            sprite[0].SetValue(Canvas.TopProperty, pos.y * 32.0);
            sprite[0].SetValue(Canvas.LeftProperty, pos.x * 32.0);
            sprite[0].Stretch = Stretch.None;
            isActive = true;
            isEmpty = false;
        }

        public void move(Direction d)//currently has no use
        {
            switch (d)
            {
                case Direction.UP:
                    break;
                case Direction.DOWN:
                    break;
                case Direction.LEFT:
                    break;
                case Direction.RIGHT:
                    break;
            }
        }

        public Image getImage()
        {
            return sprite[it];
        }

        public void setImagePosition(Point pos)
        {
            sprite[it].SetValue(Canvas.TopProperty, pos.y * 32.0);
            sprite[it].SetValue(Canvas.LeftProperty, pos.x * 32.0);
        }

        public void draw(Canvas c)
        {
            if (!isEmpty)
            {
                c.Children.Remove(sprite[it]);
                c.Children.Add(sprite[it]);
            }
        }
    }
}
