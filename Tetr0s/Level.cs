﻿//Evan Gordon
/*
 * This class manages level specific information
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetr0s
{
    class Level
    {
        private bool levelIncreased = false;
        private int levelNum = 1;
        public int LevelNum
        {
            get { return levelNum; }
            set { levelNum += value; levelIncreased = true; }
        }

        private int score = 0;
        public int Score
        {
            get { return score; }
        }

        private int completedRows = 0;
        public int CompletedRows
        {
            get { return completedRows; }
            set 
            {
                calcScore(value);
                completedRows += value;
                if (completedRows >= 10)
                {
                    levelNum++;
                    completedRows = completedRows % 10;
                }
            }
        }

        public Level()
        { }

        public Level(int lvl, int score)
        {
            levelNum = lvl;
            this.score = score;
        }
        public void calcScore(int newPoints)
        {
            int result = (100 * newPoints) * levelNum;
            if (newPoints > 1)
            {
                result += ((newPoints - 1) * 50) * levelNum;
            }

            score += result;
        }
        public bool checkIfLevelIncreased()
        {
            bool temp = levelIncreased;
            levelIncreased = false;
            return temp;
        }
    }
}
