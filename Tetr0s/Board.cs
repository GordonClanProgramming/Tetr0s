﻿//Evan Gordon
/*
 * This clas file defines what a point is, as well as containing and managing all of the data related to the game board
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tetr0s
{
    enum Shape{SQUARE, LBLOCK, RLBLOCK, LINE, TBLOCK, ZBLOCK, RZBLOCK};//used for definine which shape a group will take
    class Point
    {
        public int x = -1, y = -1;

        public Point()
        {
            x = -1;
            y = -1;
        }
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    class Board
    {
        private Tile[,] board = new Tile[10, 18];
        private string[,] fileNames = new string[1, 6];
        private  string path;
        private bool gameOver = false;
        public bool GameOver
        {
            get { return gameOver; }
        }
        
        private Random rndNum = new Random();
        Queue<TileShape> shapeStack = new Queue<TileShape>();// add in logic later
        TileShape currentTileShape = new TileShape();

        public Board(Canvas c)
        {
            gameOver = false;
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    board[i, j] = new Tile();
                }
            }
            //this is for showing next elements
            //shapeStack.Enqueue(new TileShape());
        }

        public string setPath(string s)
        {
            return System.IO.Path.Combine(Environment.CurrentDirectory, s);
        }

        public string setColor(Shape s)
        {
            switch (s)
            {
                case Shape.SQUARE:
                    return "basicBlockYellow.png";
                case Shape.LBLOCK:
                    return "basicBlockOrange.png";
                case Shape.RLBLOCK:
                    return "basicBlockBlue.png";
                case Shape.LINE:
                    return "basicBlockLightBlue.png";
                case Shape.TBLOCK:
                    return "basicBlockPurple.png";
                case Shape.ZBLOCK:
                    return "basicBlockRed.png";
                case Shape.RZBLOCK:
                    return "basicBlockGreen.png";
            }
            return "";//maybe have error throw?
        }

        public void spawn(Canvas c)
        {            
            Shape s = (Shape)rndNum.Next(0, 6); // add back in later
            currentTileShape.spawn(s, generateRotation(s), rndNum.Next(0, 10), ref board);
            path = setPath(setColor(currentTileShape.CurrShape));
            Point[] arr = currentTileShape.getLocationsArray();
            foreach (Point po in arr)
            {
                if (!board[po.x, po.y].IsEmpty)//check to make sure all locations of tileShape are empty
                {
                    gameOver = true;
                    return;
                }
                board[po.x, po.y] = new Tile(path, po);//spawn new block
            }
            foreach (Point po in arr)//this is seperate so that it only shows up if all points are valid
            {
                c.Children.Add(board[po.x, po.y].getImage());
            }
        }

        public int generateRotation(Shape s)
        {
            int rotationNum = 0;
            switch (s)
            {
                case Shape.SQUARE:
                    //no rand needed because it has no rotations
                    break;
                case Shape.LBLOCK:
                    rotationNum = rndNum.Next(0, 4);
                    break;
                case Shape.RLBLOCK:
                    rotationNum = rndNum.Next(0, 4);
                    break;
                case Shape.LINE:
                    rotationNum = rndNum.Next(0, 2);
                    break;
                case Shape.TBLOCK:
                    rotationNum = rndNum.Next(0, 4);
                    break;
                case Shape.ZBLOCK:
                    rotationNum = rndNum.Next(0, 2);
                    break;
                case Shape.RZBLOCK:
                    rotationNum = rndNum.Next(0, 2);
                    break;
            }
            return rotationNum;

        }

        public bool rotate(Turn t, Canvas c)
        {
            Point[] prevLoc = currentTileShape.getLocationsArray();
            if (currentTileShape.rotate(t, ref board))
            {
                foreach (Point po in prevLoc)//remove old location
                {
                    c.Children.Remove(board[po.x, po.y].getImage());
                    board[po.x, po.y] = new Tile();
                }
                Point[] newLoc = currentTileShape.getLocationsArray();
                foreach (Point po in newLoc)
                {
                    board[po.x, po.y] = new Tile(path, po);
                    board[po.x, po.y].setImagePosition(po);
                    c.Children.Add(board[po.x, po.y].getImage());
                }
                return true;
            }
            else
            {
                //play invalid sound
                return false;
            }
        }

        public bool move(Direction d, Canvas c)
        {
            Point[] prevLoc = currentTileShape.getLocationsArray();
            if (currentTileShape.move(d, ref board))
            {
                foreach (Point po in prevLoc)//remove old location
                {
                    c.Children.Remove(board[po.x, po.y].getImage());
                    board[po.x, po.y] = new Tile();
                }
                Point[] newLoc = currentTileShape.getLocationsArray();
                foreach (Point po in newLoc)
                {
                    board[po.x, po.y] = new Tile(path, po);
                    board[po.x, po.y].setImagePosition(po);
                    c.Children.Add(board[po.x, po.y].getImage());
                }
                return true;
            }
            else
            {
                //play invalid sound
                return false;
            }
        }

        public void maxLower(Canvas c)
        {
            Point[] arr = currentTileShape.getLocationsArray();
            foreach (Point po in arr)//remove images
            {
                c.Children.Remove(board[po.x, po.y].getImage());
                board[po.x, po.y] = new Tile();
            }
            while (!currentTileShape.gravity(ref board)) ;//move until collision

            Point[] newPositions = currentTileShape.getLocationsArray();
            foreach (Point po in newPositions)
            {
                board[po.x, po.y] = new Tile(path, po);
                board[po.x, po.y].setImagePosition(po);//accepts indexes
                c.Children.Add(board[po.x, po.y].getImage());
            }

        }
        public int update(Canvas c, Point[] arr)//returns value from removeFullRows
        {
            if (!currentTileShape.gravity(ref board))
            {
                foreach (Point po in arr)
                {
                    board[po.x, po.y] = new Tile();//empty prev tile
                }
                Point[] newPositions = currentTileShape.getLocationsArray();
                foreach(Point po in newPositions)
                {
                    board[po.x, po.y] = new Tile(path ,po);
                    board[po.x, po.y].setImagePosition(po);//accepts indexes
                }
            }
            else//collision occured so spawn new shape
            {
                foreach (Point po in arr)
                {
                    c.Children.Add(board[po.x, po.y].getImage());//let old tile stick around
                    board[po.x, po.y].IsActive = false;
                    board[po.x, po.y].IsEmpty = false;
                }
                spawn(c);
                if (!gameOver)
                {
                    Point[] newPositions = currentTileShape.getLocationsArray();
                    foreach (Point po in newPositions)
                    {
                        c.Children.Remove(board[po.x, po.y].getImage());//remove new tiles from canvas so draw func can re-add it
                    }
                }
            }

            return removeFullRows(c);
        }

        public int removeFullRows(Canvas c)//returns number of rows that were deleted
        {
            int result = 0;
            for (int j = 0; j < board.GetLength(1); j++)
            {
                if (!board[0, j].IsEmpty && !board[0, j].IsActive)
                {
                    bool fullRow = true;
                    for (int i = 1; i < board.GetLength(0); i++)
                    {
                        if (board[i, j].IsEmpty || board[i, j].IsActive)
                        {
                            fullRow = false;
                        }
                    }
                    if (fullRow)
                    {
                        result++;
                        deleteRow(j, c);//call delete row
                    }
                }
            }
            return result;
        }

        public void deleteRow(int rIndex, Canvas c)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                c.Children.Remove(board[i, rIndex].getImage());
                board[i, rIndex] = new Tile();
            }
            if(rIndex != 0)//no need to move row -1 down (wara)
            {
                for (int j = rIndex - 1; j >= 0; j--)
                {
                    for (int i = 0; i < board.GetLength(0); i++)
                    {
                        if (!board[i, j].IsEmpty && !board[i, j].IsActive)
                        {
                            c.Children.Remove(board[i, j].getImage());
                            board[i, j + 1] = board[i, j];
                            Point temp = new Point();
                            temp.x = i;
                            temp.y = j + 1;
                            board[i, j + 1].setImagePosition(temp);
                            board[i, j] = new Tile();
                            c.Children.Add(board[i, j + 1].getImage());
                            //lol delete from canvas
                        }
                    }
                }
            }
        }

        public int draw(Canvas c)
        {
            Point[] positions = currentTileShape.getLocationsArray();
            foreach (Point po in positions)
            {
                c.Children.Remove(board[po.x, po.y].getImage());
            }

            int r = update(c, positions);

            if (!gameOver)
            {
                positions = currentTileShape.getLocationsArray();
                foreach (Point po in positions)
                {
                    c.Children.Add(board[po.x, po.y].getImage());
                }
            }
            return r;
        }

        public void save(ref StreamWriter writer)
        {
            for (int j = 0; j < board.GetLength(1); j++)
            {
                string result = "";
                for (int i = 0; i < board.GetLength(0); i++)
                {
                    if (!board[i, j].IsEmpty && !board[i, j].IsActive)
                    {
                        result += "1";
                    }
                    else
                    {
                        result += "0";
                    }
                    if (i < board.GetLength(0) - 1)
                    {
                        result += ",";
                    }
                }
                writer.WriteLine(result);
            }
        }

        public void clear(Canvas c)
        {
            for(int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (!board[i, j].IsEmpty)
                    {
                        c.Children.Remove(board[i, j].getImage());
                        board[i, j] = new Tile();
                    }
                }
            }
        }

        public void load(ref StreamReader reader, Canvas c)
        {
            //http://stackoverflow.com/questions/7425963/how-to-read-values-from-a-comma-separated-file
            string line = "";
            int i = 0, j = 0;
            path = setPath(setColor(Shape.LINE));

            while ((line = reader.ReadLine()) != null)
            {
                line.Trim();
                string[] tiles = line.Split(',');
                for (i = 0; i < board.GetLength(0); i++)
                {
                    if(tiles[i] == "1")
                    {
                        board[i, j] = new Tile(path, new Point(i, j));
                        board[i, j].IsActive = false;
                        board[i, j].IsEmpty = false;
                        c.Children.Add(board[i, j].getImage());
                    }
                    else
                    {
                        board[i,j] = new Tile();
                    }
                }
                j++;
            }
        }
    }
}
