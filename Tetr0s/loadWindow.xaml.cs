﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.IO;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tetr0s
{
    /// <summary>
    /// Interaction logic for loadWindow.xaml
    /// </summary>
    public partial class loadWindow : Window
    {
        private string text;
        public string Text
        {
            get { return text; }
        }

        public loadWindow()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {

            if (fileText.Text != "" && fileText.Text.All(char.IsLetterOrDigit) && File.Exists(fileText.Text + ".txt"))
            {
                text = fileText.Text;
                this.Close();
            }
        }
    }
}
