﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;

namespace Tetr0s
{
    enum Turn{LEFT, RIGHT};
    class ShapeLocations
    {
        public int rotationI = 0;
        public Point centerTile = new Point();
        public Point[] centerChildren = new Point[3];

        public ShapeLocations()
        {
        }
        public ShapeLocations(Point center)
        {
            centerTile = center;
        }
    }

    class TileShape
    {
        private Shape currShape = Shape.SQUARE;
        public Shape CurrShape
        {
            get { return currShape; }
        }

        private ShapeLocations sLoc = new ShapeLocations();
        public ShapeLocations SLoc
        {
            get { return sLoc; }
        }


        public void spawn(Shape s, int rotation, int xLoc, ref Tile[,] b)//accepts randoms for rotation, and xLoc
        {
            currShape = s;
            Point tempPoint = new Point(xLoc, 1);
            if(( s == Shape.LBLOCK && rotation == 1) || (s == Shape.RLBLOCK && rotation == 3) ||(s == Shape.LINE && rotation == 1) ||
                (s == Shape.TBLOCK && rotation == 2))//special spawn location cases for certain rotations
            {
                tempPoint.y += -1;
            }
            sLoc = calcIndecies(tempPoint, s, rotation);
            bool isValid = false;
            int tempX = xLoc;//this is used for the incrementing and decrementing nessisary in the while loop
            while (!isValid)
            {
                if(isWithinBoard( sLoc, ref b))
                {
                    isValid = true;
                }
                else if (sLoc.centerTile.x > b.GetLength(0)/2)//if it's not spawned in a valid location move l/r based on center val
                {
                    tempX += -1;
                    sLoc = calcIndecies(new Point(tempX, 1), s, rotation);
                }
                else
                {
                    tempX += 1;
                    sLoc = calcIndecies(new Point(tempX, 1), s, rotation);
                }
            }
            //anything else needed?
        }

        public bool gravity(ref Tile[,] b)//honestly just couldn't come up w a better name for this func, returns true if there was collision
        {
            ShapeLocations newLoc = calcIndecies(new Point(sLoc.centerTile.x, sLoc.centerTile.y + 1), currShape, sLoc.rotationI);

            if (!isWithinBoard(newLoc, ref b) || collision(newLoc, ref b)) //if collision or !within board, return true,
            {
                return true;
            }
            else//move sLoc down
            {
                sLoc = newLoc;
                return false;
            }
        }

        public bool rotate(Turn t, ref Tile[,] b)
        {
            int newRotationIndex = calcRotationIndex(t);
            ShapeLocations newLoc = calcIndecies(sLoc.centerTile, currShape, newRotationIndex);

            if(isWithinBoard(newLoc, ref b) && !collision(newLoc, ref b))
            {
                sLoc = newLoc;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool move(Direction d, ref Tile[,] b)
        {
            ShapeLocations newLoc;
            if (d == Direction.LEFT)
            {
                newLoc = calcIndecies(new Point(sLoc.centerTile.x - 1, sLoc.centerTile.y), currShape, sLoc.rotationI);
            }
            else
            {
                newLoc = calcIndecies(new Point(sLoc.centerTile.x + 1, sLoc.centerTile.y), currShape, sLoc.rotationI);
            }

            if (isWithinBoard(newLoc, ref b) && !collision(newLoc, ref b))
            {
                sLoc = newLoc;
                return true;
            }
            else
            {
                return false;
            }

        }

        public Point[] getLocationsArray()//used only for getting locations when shape needs to be drawn
        {
            Point[] arr = new Point[4];
            arr[0] = sLoc.centerTile;
            arr[1] = sLoc.centerChildren[0];
            arr[2] = sLoc.centerChildren[1];
            arr[3] = sLoc.centerChildren[2];
            return arr;
        }

        //honestly due to time constraints im leaving this function as is, there are cleaner more optimized ways of doing this
        //but this will suffice for now.
        //returns location of new Shape based on center that was given, only works with square currently
        public ShapeLocations calcIndecies(Point center, Shape s, int rotation)//this might turn into a mother of a func
        {
            //this func only calc the values of the shape at given location, check if they are valid later
            ShapeLocations newLoc = new ShapeLocations(center);
            newLoc.rotationI = rotation;

            switch (s)//i hate hard coding things like this, but it had to be done :<
            {
                case Shape.SQUARE:
                    //can't rotate
                    newLoc.centerChildren[0] = new Point(center.x + 1, center.y);
                    newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                    newLoc.centerChildren[2] = new Point(center.x + 1, center.y - 1);
                    return newLoc;
                case Shape.LBLOCK:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y + 1);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y + 1);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y + 1);
                        newLoc.centerChildren[1] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y);
                    }
                    else if (rotation == 2)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    else if (rotation == 3)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[1] = new Point(center.x + 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y - 1);
                    }
                    break;
                case Shape.RLBLOCK:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y + 1);
                        newLoc.centerChildren[2] = new Point(center.x - 1, center.y + 1);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y);
                    }
                    else if (rotation == 2)
                    {
                        newLoc.centerChildren[0] = new Point(center.x + 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    else if (rotation == 3)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[1] = new Point(center.x + 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y + 1);
                    }
                    break;
                case Shape.LINE:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y + 1);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 2);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[1] = new Point(center.x + 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x + 2, center.y);
                    }
                    break;
                case Shape.TBLOCK:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x + 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    else if (rotation == 2)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[1] = new Point(center.x, center.y + 1);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y);
                    }
                    else if (rotation == 3)
                    {
                        newLoc.centerChildren[0] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    break;
                case Shape.ZBLOCK:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[2] = new Point(center.x + 1, center.y);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x + 1 , center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x + 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    break;
                case Shape.RZBLOCK:
                    if (rotation == 0)
                    {
                        newLoc.centerChildren[0] = new Point(center.x + 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x, center.y - 1);
                        newLoc.centerChildren[2] = new Point(center.x - 1, center.y);
                    }
                    else if (rotation == 1)
                    {
                        newLoc.centerChildren[0] = new Point(center.x - 1, center.y - 1);
                        newLoc.centerChildren[1] = new Point(center.x - 1, center.y);
                        newLoc.centerChildren[2] = new Point(center.x, center.y + 1);
                    }
                    break;
            }
            return newLoc;
        }

        public bool collision(ShapeLocations locs, ref Tile[,] b)//returns true if collision occurs
        {
            if (pointCollision(locs.centerTile, ref b) || pointCollision(locs.centerChildren[0], ref b) ||
                    pointCollision(locs.centerChildren[1], ref b) || pointCollision(locs.centerChildren[2], ref b))
            {
                return true;
            }
            return false;
        }

        public bool pointCollision(Point p, ref Tile[,] b)//returns true when collision with non active tile
        {
            if (!b[p.x, p.y].IsActive && !b[p.x, p.y].IsEmpty)
            {
                return true;
            }
            return false;
        }

        public bool isWithinBoard(ShapeLocations locs, ref Tile[,] b)
        {
            if (pointIsWithinBoard(locs.centerTile, ref b) && pointIsWithinBoard(locs.centerChildren[0], ref b) &&
                    pointIsWithinBoard(locs.centerChildren[1], ref b) && pointIsWithinBoard(locs.centerChildren[2], ref b))
            {
                return true;
            }
            return false;
        }

        public bool pointIsWithinBoard(Point p, ref Tile[,] b)//helper func for isWithinBoard
        {
            if (p.x >= 0 && p.x < b.GetLength(0) && p.y >= 0 && p.y < b.GetLength(1))
            {
                return true;
            }
            return false;
        }

        public int calcRotationIndex(Turn direction)
        {
            int result;
            if (direction == Turn.LEFT)
            {
                result = sLoc.rotationI + 1;
            }
            else
            {
                result = sLoc.rotationI - 1;
                if (result < 0)
                {
                    result = maxShapeRotationIndex(currShape);
                }
            }
            switch (currShape)
            {
                case Shape.SQUARE:
                    //can't rotate
                    return 0;
                case Shape.LBLOCK:
                    result = result % 4;
                    return result;
                case Shape.RLBLOCK:
                    result = result % 4;
                    return result;
                case Shape.LINE:
                    result = result % 2;
                    return result;
                case Shape.TBLOCK:
                    result = result % 4;
                    return result;
                case Shape.ZBLOCK:
                    result = result % 2;
                    return result;
                case Shape.RZBLOCK:
                    result = result % 2;
                    return result;
            }
            return 0;
        }

        private int maxShapeRotationIndex(Shape s)
        {
            switch (s)
            {
                case Shape.SQUARE:
                    //can't rotate
                    return 0;
                case Shape.LBLOCK:
                    return 3;
                case Shape.RLBLOCK:
                    return 3;
                case Shape.LINE:
                    return 1;
                case Shape.TBLOCK:
                    return 3;
                case Shape.ZBLOCK:
                    return 1;
                case Shape.RZBLOCK:
                    return 1;
            }
            return 0;
        }

    }
}
